# 1. django-docker项目说明
- 使用docker-compose部署：django+nginx+uwsgi+celery+redis+mysql
- 博客地址：https://www.cnblogs.com/xiaonq/p/12377099.html
- 本项目参考项目：https://github.com/huchenw/django-docker.git
## 1.1 安装docker & docker-compose
> 参考官方

- Install Docker Engine from the tutorial <https://docs.docker.com/engine/installation/>.</br>
- Install Docker Compose from the tutorial <https://docs.docker.com/compose/install/>.</br>
Get the latest project clone to your computer:
> 参考博客
- docker安装使用: https://www.cnblogs.com/xiaonq/p/10241045.html
- docker-compose安装使用: https://www.cnblogs.com/xiaonq/p/10256414.html

## 1.2 快速使用
```bash
$  git clone https://gitee.com/edushiyanlou/django-docker.git
$  docker-compose up -d        # 启动所有容器
$  docker-compose logs celery  # 查看celery运行日志
$  docker-compose down         # 关闭所有容器
启动后访问：http://192.168.56.11
```

## 1.3 如果使用前后端不分离的项目需要收集Static Files给nginx访问
```bash
$ docker-compose exec web bash
$ python manage.py collectstatic
```
## 1.4 Django Admin
If you want to access django admin site, please apply the django default migrations to database:
```bash
$ docker-compose exec web bash
$ python manage.py migrate
```
Then you need to create a superuser account:
```bash
$ python manage.py createsuperuser
$ ...
```

## 1.5 查看docker镜像的使用方法

| Name   | Image                              |
| ------ | ---------------------------------- |
| Nginx  | <https://hub.docker.com/_/nginx/>  |
| MySQL  | <https://hub.docker.com/_/mysql/>  |
| Redis  | <https://hub.docker.com/_/redis/>  |
| Python | <https://hub.docker.com/_/python/> |


# 2. 不使用docker测试项目的运行
## 2.1 说明
### 2.1.1 设置项目访问的服务DNS解析

```python
[root@linux-node1 web]# vim /etc/hosts
192.168.56.1 mysql
192.168.56.11 redis
```

### 2.1.2 配置mysql账号和密码

```python
# 1、创建用户
create user 'django'@'%' identified by 'django';
create database djangodocker charset utf8;

# 2、授予mup用户授予对mup数据库的操作权限
GRANT ALL ON djangodocker.* TO 'django'@'%';
flush privileges;
select host,user from mysql.user;
```

### 2.1.3 安装依赖包
```python
[root@linux-node1 django-docker]# yum install mysql-devel  # 安装mysl-dev避免安装mysqlclient报错
[root@linux-node1 django-docker]# pip3 install -r requirements.txt 
```

### 2.1.4 启动项目

```python
[root@linux-node1 web]#  cd /code/django-docker/web
[root@linux-node1 web]#  celery -A web worker -l info
[root@linux-node1 web]#  python3 manage.py runserver 0.0.0.0:8000
http://192.168.56.11:8000/
```

